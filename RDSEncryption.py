import logging
import boto3
import sys
import time
from botocore.exceptions import ClientError
from botocore.exceptions import WaiterError
import argparse
import multiprocessing
import mysql.connector


def main(unencrypted_instance, profile, alias, downtime, password):
    # use try block to catch all exceptions and errors
    try:
        # create a new session for each rds instance
        if profile:
            rds_session = boto3.session.Session(profile_name=profile)
        else:
            rds_session = boto3.session.Session()

        unencrypted_client = rds_session.client('rds')

        # create waiters for instance creation and snapshots
        waiter_snapshot_available = unencrypted_client.get_waiter('db_snapshot_available')
        waiter_instance_available = unencrypted_client.get_waiter('db_instance_available')
        waiter_instance_deleted = unencrypted_client.get_waiter('db_instance_deleted')
        waiter_snapshot_deleted = unencrypted_client.get_waiter('db_snapshot_deleted')

        if not downtime:

            print("--Creating Read Replica of RDS Instance ({})--".format(unencrypted_instance['DBInstanceIdentifier']))

            unencrypted_client.create_db_instance_read_replica(
                DBInstanceIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + "-rr",
                SourceDBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier'],
                MultiAZ=unencrypted_instance['MultiAZ'],
                AvailabilityZone=unencrypted_instance['AvailabilityZone']
            )

            # wait for the read replica to become available
            waiter_instance_available.wait(
                DBInstanceIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + "-rr"
            )

            rrResponse = unencrypted_client.describe_db_instances(
                DBInstanceIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + "-rr"
            )

            tempInstance = None

            for rrInstance in rrResponse['DBInstances']:
                tempInstance = rrInstance

            print("--Connecting to Read Replica Instance ({})--".format(tempInstance['DBInstanceIdentifier']))

            try:
                # connect to the read replica
                conn = mysql.connector.connect(
                    host=tempInstance['Endpoint']['Address'],
                    user=tempInstance['MasterUsername'],
                    passwd=password,
                    port=int(tempInstance['Endpoint']['Port']),
                    database=unencrypted_instance['DBName'],
                )

                cur = conn.cursor()

                complete = False
                while not complete:
                    cur.execute("SHOW SLAVE STATUS")
                    if cur.fetchall()[0][-7] == 0:
                        complete = True
                    else:
                        time.sleep(15)
                        print("--Waiting for Read Replica ({}) to be in sync with Instance ({})--"
                              .format(tempInstance['DBInstanceIdentifier'],
                                      unencrypted_instance['DBInstanceIdentifier']))

                # try to run two commands/print results and then disconnect for testing
                cur.execute("CALL mysql.rds_stop_replication;", multi=True)
                cur.fetchall()
                cur.execute("SHOW SLAVE STATUS;")
                slave_list = cur.fetchall()
                master_log_file = slave_list[0][9]
                exec_master_log_pos = slave_list[0][21]

                # create snapshots from the read replica
                create_rr_snapshots(alias, tempInstance, unencrypted_client, unencrypted_instance,
                                    waiter_snapshot_available)

                print("--Creating RDS Read Replica Instance from encrypted Snapshot ({})--".format(
                    'new-encrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr-snapshot'))

                # make sure encrypted instance is in same security group as others
                vpc_list = []
                for vpc_id in unencrypted_instance['VpcSecurityGroups']:
                    vpc_list.append(vpc_id['VpcSecurityGroupId'])

                # create a new rds instance from the encrypted snapshot
                unencrypted_client.restore_db_instance_from_db_snapshot(
                    DBInstanceIdentifier='encrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr',
                    DBSnapshotIdentifier='new-encrypted-' + unencrypted_instance[
                        'DBInstanceIdentifier'] + '-rr-snapshot',
                    DBSubnetGroupName=unencrypted_instance['DBSubnetGroup']['DBSubnetGroupName'],
                    MultiAZ=unencrypted_instance['MultiAZ'],
                    AvailabilityZone=unencrypted_instance['AvailabilityZone'],
                    VpcSecurityGroupIds=vpc_list,
                    PubliclyAccessible=True
                )

                # wait for the encrypted instance to become available
                waiter_instance_available.wait(
                    DBInstanceIdentifier='encrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr'
                )

                # modify the security group associated with the unencrypted rds instance to allow traffic from
                # the new encrypted read replica
                ec2 = boto3.client('ec2')
                vpc_security_groups = unencrypted_instance['VpcSecurityGroups']
                security_group = None
                for s in vpc_security_groups:
                    security_group = s

                try:
                    ec2.authorize_security_group_ingress(
                        GroupId=security_group['VpcSecurityGroupId'],
                        IpPermissions=[
                            {
                                'IpProtocol': 'tcp',
                                'FromPort': unencrypted_instance['Endpoint']['Port'],
                                'ToPort': unencrypted_instance['Endpoint']['Port'],
                                'UserIdGroupPairs': [
                                    {
                                        'GroupId': security_group['VpcSecurityGroupId']
                                    }
                                ]
                            }
                        ]
                    )
                except ClientError as e:
                    if e.response['Error']['Code'] != 'InvalidPermission.Duplicate':
                        print(e.response['Error']['Message'])
                        print("Stopping Encryption for RDS Instance ({})".format(
                            unencrypted_instance['DBInstanceIdentifier']))
                        sys.exit()
                    logging.info(e)

                enc_response = unencrypted_client.describe_db_instances(
                    DBInstanceIdentifier='encrypted-' + unencrypted_instance['DBInstanceIdentifier'] + "-rr"
                )

                for rrInstance in enc_response['DBInstances']:
                    tempInstance = rrInstance

                print("--Connecting to Instance ({})--".format(tempInstance['DBInstanceIdentifier']))

                enc_conn = mysql.connector.connect(
                    host=tempInstance['Endpoint']['Address'],
                    user=tempInstance['MasterUsername'],
                    passwd=password,
                    port=int(tempInstance['Endpoint']['Port']),
                    database=unencrypted_instance['DBName'],
                )

                enc_curr = enc_conn.cursor()

                enc_curr.execute("CALL mysql.rds_set_external_master(\'{}\',{},\'{}\',\'{}\',\'{}\',{},{});".format(
                    unencrypted_instance['Endpoint']['Address'],
                    unencrypted_instance['Endpoint']['Port'],
                    tempInstance['MasterUsername'],
                    password,
                    master_log_file,
                    exec_master_log_pos,
                    0
                ), multi=True)

                enc_curr.execute("CALL mysql.rds_start_replication;", multi=True)

                if cur.fetchall():
                    complete = False
                    while not complete:
                        cur.execute("SHOW SLAVE STATUS")
                        if cur.fetchall()[0][-7] == 0:
                            complete = True
                        else:
                            time.sleep(15)
                            print("--Waiting for Read Replica ({}) to be in sync with Instance ({})--"
                                  .format(tempInstance['DBInstanceIdentifier'],
                                          unencrypted_instance['DBInstanceIdentifier']))

                cur.execute("CALL mysql.rds_start_replication;", multi=True)

                print("--Promoting Read Replica ({})--".format(
                    'encrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr'))
                enc_curr.execute("CALL mysql.rds_reset_external_master", multi=True)

                print("--Changing name of unencrypted RDS Instance ({}) --> ({})"
                      .format(unencrypted_instance['DBInstanceIdentifier'],
                              unencrypted_instance['DBInstanceIdentifier'] + '-old'))

                unencrypted_client.modify_db_instance(
                    DBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier'],
                    ApplyImmediately=True,
                    NewDBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier'] + '-old'
                )

                time.sleep(60)

                # wait for the encrypted instance to become available
                complete = False
                while not complete:
                    try:
                        waiter_instance_available.wait(
                            DBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier'] + '-old'
                        )
                        complete = True
                    except WaiterError as e:
                        print("--Waiting for Instance to Rename--")
                        time.sleep(30)

                print("--Changing name of unencrypted RDS Instance ({}) --> ({})"
                      .format('encrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr',
                              unencrypted_instance['DBInstanceIdentifier']))

                unencrypted_client.modify_db_instance(
                    DBInstanceIdentifier='encrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr',
                    ApplyImmediately=True,
                    NewDBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier']
                )

                time.sleep(60)

                # wait for the encrypted instance to become available
                complete = False
                while not complete:
                    try:
                        waiter_instance_available.wait(
                            DBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier']
                        )
                        complete = True
                    except WaiterError as e:
                        print("--Waiting for Instance to Rename--")
                        time.sleep(30)

                print('--Cleaning up Snapshots--')
                print("--Deleting unencrypted Read ReplicaInstance ({})--"
                      .format('unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr'))

                # delete the old unencrypted rds instance without a final snapshot
                unencrypted_client.delete_db_instance(
                    DBInstanceIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr',
                    SkipFinalSnapshot=True
                )

                # wait for the instance to be deleted
                waiter_instance_deleted.wait(
                    DBInstanceIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr',
                )

                print("--Deleting Snapshot ({})--".format(
                    'new-encrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr-snapshot'))

                # delete the encrypted snapshot
                unencrypted_client.delete_db_snapshot(
                    DBSnapshotIdentifier='new-encrypted-' + unencrypted_instance[
                        'DBInstanceIdentifier'] + '-rr-snapshot'
                )

                # wait for the encrypted snapshot to be deleted
                waiter_snapshot_deleted.wait(
                    DBSnapshotIdentifier='new-encrypted-' + unencrypted_instance[
                        'DBInstanceIdentifier'] + '-rr-snapshot',
                    WaiterConfig={
                        'Delay': 5,
                        'MaxAttempts': 30
                    }
                )

                print("--Deleting Snapshot ({})--".format(
                    'unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + 'rr-snapshot'))

                # delete the encrypted snapshot
                unencrypted_client.delete_db_snapshot(
                    DBSnapshotIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr-snapshot'
                )

                # wait for the encrypted snapshot to be deleted
                waiter_snapshot_deleted.wait(
                    DBSnapshotIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr-snapshot',
                    WaiterConfig={
                        'Delay': 5,
                        'MaxAttempts': 30
                    }
                )

                conn.disconnect()
                enc_conn.disconnect()
                sys.exit()

            except Exception as e:
                print('Database connection failed: {}'.format(e))
                print('Stopping encryption for RDS Instance ({})'.format(unencrypted_instance['DBInstanceIdentifier']))
                sys.exit()

        # don't create read replica for the db
        else:

            print("--Creating Snapshot for RDS Instance ({})--".format(unencrypted_instance['DBInstanceIdentifier']))

            # create an unencrypted snapshot of the rds instance
            unencrypted_client.create_db_snapshot(
                DBSnapshotIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'],
                DBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier']
            )

            # wait for the snapshot to finish
            waiter_snapshot_available.wait(
                DBSnapshotIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'],
                DBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier']
            )

            print("--Creating encrypted copy of Snapshot ({})--".format(
                'unencrypted-' + unencrypted_instance['DBInstanceIdentifier']))

            keyid, keyarn = getKMSKey(alias)
            if keyid is None:
                sys.exit()
            else:
                logging.info('Retrieved existing AWS KMS CMK')

            # create an encrypted copy of the instance with the kms id from the unencrypted snapshot
            # use of the kms id automatically encrypts the snapshot
            unencrypted_client.copy_db_snapshot(
                SourceDBSnapshotIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'],
                TargetDBSnapshotIdentifier='new-encrypted-' + unencrypted_instance['DBInstanceIdentifier'],
                KmsKeyId=keyarn,
                CopyTags=True
            )

            # wait for the encrypted snapshot to become available
            waiter_snapshot_available.wait(
                DBSnapshotIdentifier='new-encrypted-' + unencrypted_instance['DBInstanceIdentifier'],
                DBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier']
            )

            print("--Creating RDS Instance from encrypted Snapshot ({})--".format(
                'encrypted-' + unencrypted_instance['DBInstanceIdentifier']))

            # create a new rds instance from the encrypted snapshot
            unencrypted_client.restore_db_instance_from_db_snapshot(
                DBInstanceIdentifier='encrypted-' + unencrypted_instance['DBInstanceIdentifier'],
                DBSnapshotIdentifier='new-encrypted-' + unencrypted_instance['DBInstanceIdentifier'],
                DBSubnetGroupName=unencrypted_instance['DBSubnetGroup']['DBSubnetGroupName'],
                MultiAZ=unencrypted_instance['MultiAZ'],
                AvailabilityZone=unencrypted_instance['AvailabilityZone']
            )

            # wait for the encrypted instance to become available
            waiter_instance_available.wait(
                DBInstanceIdentifier='encrypted-' + unencrypted_instance['DBInstanceIdentifier']
            )

            print('--Cleaning up old Snapshots and Instances--')

            print('--Deleting Snapshot ({})--'.format('unencrypted-' + unencrypted_instance['DBInstanceIdentifier']))

            # delete the unencrypted snapshot
            unencrypted_client.delete_db_snapshot(
                DBSnapshotIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier']
            )

            # wait for the unencrypted snapshot to be deleted
            waiter_snapshot_deleted.wait(
                DBSnapshotIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'],
                WaiterConfig={
                    'Delay': 5,
                    'MaxAttempts': 30
                }
            )

            print("--Deleting Snapshot ({})--".format('new-encrypted-' + unencrypted_instance['DBInstanceIdentifier']))

            # delete the encrypted snapshot
            unencrypted_client.delete_db_snapshot(
                DBSnapshotIdentifier='new-encrypted-' + unencrypted_instance['DBInstanceIdentifier']
            )

            # wait for the encrypted snapshot to be deleted
            waiter_snapshot_deleted.wait(
                DBSnapshotIdentifier='new-encrypted-' + unencrypted_instance['DBInstanceIdentifier'],
                WaiterConfig={
                    'Delay': 5,
                    'MaxAttempts': 30
                }
            )

            print("--Stopping RDS Instance ({})".format(unencrypted_instance['DBInstanceIdentifier']))

            unencrypted_client.stop_db_instance(
                DBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier']
            )

            print("--Deleting unencrypted Instance ({})--".format(unencrypted_instance['DBInstanceIdentifier']))

            # delete the old unencrypted rds instance without a final snapshot
            unencrypted_client.delete_db_instance(
                DBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier'],
                SkipFinalSnapshot=True
            )

            # wait for the instance to be deleted
            waiter_instance_deleted.wait(
                DBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier'],
            )

            print('--Renaming new encrypted Instance with original Instance name--')

            # rename the new encrypted rds instance to the old rds instance name
            unencrypted_client.modify_db_instance(
                DBInstanceIdentifier='encrypted-' + unencrypted_instance['DBInstanceIdentifier'],
                ApplyImmediately=True,
                NewDBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier']
            )

            # wait 60 seconds, otherwise throws and error for the waiter trying to find instance
            # with an unknown name
            time.sleep(60)

            # wait for the encrypted instance to become available
            complete = False
            while not complete:
                try:
                    waiter_instance_available.wait(
                        DBInstanceIdentifier=unencrypted_instance['DBInstanceIdentifier']
                    )
                    complete = True
                except WaiterError as e:
                    print("--Waiting for Instance to Rename--")
                    time.sleep(30)

    # catch all errors and exceptions and print the error
    except ClientError as e:
        print("--ERROR for RDS Instance ({})--".format(unencrypted_instance['DBInstanceIdentifier']))
        print(e.response['Error']['Message'])
        sys.exit()


def create_rr_snapshots(alias, tempInstance, unencrypted_client, unencrypted_instance, waiter_snapshot_available):
    print("--Creating Snapshot for RDS Read Replica Instance ({})--".format(tempInstance['DBInstanceIdentifier']))
    # create snapshot of read replica
    unencrypted_client.create_db_snapshot(
        DBSnapshotIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr-snapshot',
        DBInstanceIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr'
    )
    # wait for snapshot to become available
    waiter_snapshot_available.wait(
        DBSnapshotIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr-snapshot',
        DBInstanceIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr'
    )
    # create an encrypted copy of the snapshot
    print("--Creating encrypted copy of Snapshot ({})--".format(
        'unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr-snapshot'))
    keyid, keyarn = getKMSKey(alias)
    if keyid is None:
        sys.exit()
    else:
        logging.info('Retrieved existing AWS KMS CMK')
    # create an encrypted copy of the instance with the kms id from the unencrypted snapshot
    # use of the kms id automatically encrypts the snapshot
    unencrypted_client.copy_db_snapshot(
        SourceDBSnapshotIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr-snapshot',
        TargetDBSnapshotIdentifier='new-encrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr-snapshot',
        KmsKeyId=keyarn,
        CopyTags=True
    )
    # wait for the encrypted snapshot to become available
    waiter_snapshot_available.wait(
        DBSnapshotIdentifier='new-encrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr-snapshot',
        DBInstanceIdentifier='unencrypted-' + unencrypted_instance['DBInstanceIdentifier'] + '-rr'
    )


# retrieves a kms key from a provided alias
def getKMSKey(key_alias):
    if 'alias/' not in key_alias:
        key_alias = 'alias/' + key_alias

    kmsClient = boto3.client('kms')

    try:
        kresponse = kmsClient.list_keys()
    except ClientError as e:
        logging.error(e)
        return None, None

    done = False
    while not done:
        # loop over every key the alias associates with
        for key in kresponse['Keys']:
            try:
                key_info = kmsClient.list_aliases(KeyId=key['KeyArn'])
            except ClientError as e:
                logging.error(e)
                return None, None

            # return the key id and key arn if a match is found
            for alias_key in key_info['Aliases']:
                if alias_key['AliasName'] == key_alias:
                    return alias_key['TargetKeyId'], alias_key['AliasArn']

            # if no keys match, no key was found
            if not kresponse['Truncated']:
                logging.debug('A CMK with the alias was not found')
                done = True
            else:
                # if more responses are available, try the next marker
                try:
                    kresponse = kmsClient.list_keys(Marker=kresponse['NextMarker'])
                except ClientError as e:
                    logging.error(e)
                    return None, None

    return None, None


if __name__ == '__main__':
    print("---RDS Encryption of live Instances---")

    # create parser to check commands
    parser = argparse.ArgumentParser(description='RDS Encryption of live Instances')
    required = parser.add_argument_group('required arguments')
    required.add_argument('-a', '--alias', help='Customer KMS Alias to encrypt RDS instance', required=True)
    parser.add_argument('-p', '--profile', help='Customer Profile', required=False)
    parser.add_argument('-d', '--downtime',
                        help='(True/False) (True: RDS Instance will be stopped) (False: Read Replica will be created to reduce downtime) (Default is True)',
                        required=False)
    parser.add_argument('-pas', '--password',
                        help="Password for RDS Instance connection. Must be specified if downtime is False",
                        required=False)

    args = parser.parse_args()

    # make sure that if downtime is False, a password is provided
    if args.downtime == "True" or args.downtime == "true":
        tempDowntime = True
    elif args.downtime == "False" or args.downtime == "false":
        if args.password == "" or not args.password:
            print("\n--Please provide password if downtime is False--")
            sys.exit()
        else:
            tempDowntime = False
    else:
        tempDowntime = True

    # use the passed in profile if provided one
    if args.profile:
        session = boto3.session.Session(profile_name=args.profile)
    else:
        session = boto3.session.Session()

    # create a client and waiter to check if rds instances are 'available'
    rds_client = session.client('rds')

    waiter_instance_exits = rds_client.get_waiter('db_instance_available')

    unencrypted_instances = []

    # get a description of all the rds instances
    response = rds_client.describe_db_instances()

    # loop over all of the instances
    for rds_instance in response['DBInstances']:
        # add unencrypted instances to the list
        if rds_instance['StorageEncrypted'] is False:
            print("\n--RDS Instance ({})....Encrypted: NO".format(rds_instance['DBInstanceIdentifier']))
            # skip over instance if it's not 'available'
            if rds_instance['DBInstanceStatus'] != 'available':
                print('--RDS Instance ({}) cannot be encrypted, please make it \'available\''.format(
                    rds_instance['DBInstanceIdentifier']))
            else:
                unencrypted_instances.append(rds_instance)
        else:
            print("\n--RDS Instance ({})....Encrypted: YES".format(rds_instance['DBInstanceIdentifier']))

    # if the list isn't empty, start the encryption process
    if unencrypted_instances:
        print("\n--Starting RDS Encryption...")

        processes = []

        # create a new process for each rds instance
        for instance in unencrypted_instances:
            p = multiprocessing.Process(target=main,
                                        args=(instance, args.profile, args.alias, tempDowntime, args.password))
            processes.append(p)
            p.start()

        for process in processes:
            process.join()

        print("\n--Finished RDS Encryption---")

    else:
        print("\n--All Processes Complete--")
