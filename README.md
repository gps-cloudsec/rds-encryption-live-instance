# README #

This scripts encrypts RDS Instances that are currently 'available'. There is an option for downtime if that is not an issue during the encryption process. Otherwise a read replica can be made to reduce the downtime during encryption. 

### Set Up ###

* Assumes user has credentials set up in ~/.aws/credentials
* See requirements.txt for dependencies
* Script Assumes
	- RDS instance is publicly acessible unless machine running the script is in the same VPC as RDS Instance.
	- no NACL rules blocking traffic from the machine's IP
	- internet gateway attatched to the VPC

### Run ###

* Parameters
  	- '-a' -> required
		- Customer KMS Alias for the CMK to encrypt the RDS Instance. 
    - '-p' -> optional
		- Customer Profile
	- '-d' -> optional
		- (True/False) True -> RDS instance will be stopped and downtime occurs.   False -> Read replica is created to decrease downtime.
	- '-p' -> optional, required if '-d' is False
		- Password for RDS Instance connection
		
	- Example run with read replica created.
	
		- python3 RDSEncryption.py -a aws/rds -d False -pas testdb11 
		
	- Example run with no read replica created. 
	
		- python3 RDSEncryption.py -a aws/rds
		
	
